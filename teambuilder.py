import numpy as np

from poke_env.teambuilder.teambuilder import Teambuilder

from teams import teams

class RandomTeamFromPool(Teambuilder):
    def __init__(self, teams):
        self.teams = [self.join_team(self.parse_showdown_team(team)) for team in teams]

    def yield_team(self):
        #return self.teams[0]
        return np.random.choice(self.teams)

custom_builder = RandomTeamFromPool(teams.values())