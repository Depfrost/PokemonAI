# -*- coding: utf-8 -*-
import asyncio

from poke_env.player.random_player import RandomPlayer
from poke_env.player_configuration import PlayerConfiguration
from poke_env.server_configuration import ShowdownServerConfiguration

from teambuilder import custom_builder
from maxDamagePlayer import MaxDamagePlayer
from trainedRLPlayer import TrainedRLPlayer
import tensorflow as tf

async def main():
    # We create a random player
    player = TrainedRLPlayer(
        model=tf.keras.models.load_model("model_10000"),
        player_configuration=PlayerConfiguration("ushioplayspokemon", "D6jNWKzE79Hf4w2t"),
        battle_format='gen8ou',
        team=custom_builder,
        server_configuration=ShowdownServerConfiguration,
        start_timer_on_battle_start=True,
        max_concurrent_battles=4
    )

    # Sending challenges to 'your_username'
    #await player.send_challenges("your_username", n_challenges=1)

    # Accepting one challenge from any user
    await player.accept_challenges(None, 1)

    # Playing 5 games on the ladder
    #await player.ladder(50)

    # Print the rating of the player and its opponent after each battle
    #for battle in player.battles.values():
    #    print(battle.rating, battle.opponent_rating)


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())
