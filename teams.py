teams = {

'team_1' : """
Goodra (M) @ Assault Vest
Ability: Sap Sipper
EVs: 248 HP / 252 SpA / 8 Spe
Modest Nature
IVs: 0 Atk
- Dragon Pulse
- Flamethrower
- Sludge Wave
- Thunderbolt

Sylveon (M) @ Leftovers
Ability: Pixilate
EVs: 248 HP / 244 Def / 16 SpD
Calm Nature
IVs: 0 Atk
- Hyper Voice
- Mystical Fire
- Protect
- Wish

Torkoal @ Heat Rock  
Ability: Drought  
EVs: 248 HP / 136 Def / 124 SpD  
Relaxed Nature  
- Lava Plume  
- Body Press  
- Rapid Spin  
- Toxic

Toxtricity (M) @ Throat Spray
Ability: Punk Rock
EVs: 4 Atk / 252 SpA / 252 Spe
Rash Nature
- Overdrive
- Boomburst
- Shift Gear
- Fire Punch

Seismitoad (M) @ Leftovers
Ability: Water Absorb
EVs: 252 HP / 252 Def / 4 SpD
Relaxed Nature
- Stealth Rock
- Scald
- Earthquake
- Toxic

Corviknight (M) @ Leftovers
Ability: Pressure
EVs: 248 HP / 80 SpD / 180 Spe
Impish Nature
- Defog
- Brave Bird
- Roost
- U-turn
""",

'team_2' : """
Togekiss @ Leftovers
Ability: Serene Grace
EVs: 248 HP / 8 SpA / 252 Spe
Timid Nature
IVs: 0 Atk
- Air Slash
- Nasty Plot
- Substitute
- Thunder Wave

Galvantula @ Focus Sash
Ability: Compound Eyes
EVs: 252 SpA / 4 SpD / 252 Spe
Timid Nature
IVs: 0 Atk
- Sticky Web
- Thunder Wave
- Thunder
- Energy Ball

Cloyster @ Focus Sash
Ability: Skill Link
EVs: 252 Atk / 4 SpD / 252 Spe
Adamant Nature
- Icicle Spear
- Rock Blast
- Ice Shard
- Shell Smash

Sandaconda @ Focus Sash
Ability: Sand Spit
EVs: 252 Atk / 4 SpD / 252 Spe
Jolly Nature
- Stealth Rock
- Glare
- Earthquake
- Rock Tomb

Excadrill @ Focus Sash
Ability: Sand Rush
EVs: 252 Atk / 4 SpD / 252 Spe
Adamant Nature
- Iron Head
- Rock Slide
- Earthquake
- Rapid Spin

Cinccino @ Leftovers
Ability: Skill Link
EVs: 252 Atk / 4 Def / 252 Spe
Jolly Nature
- Bullet Seed
- Knock Off
- Rock Blast
- Tail Slap
""",

'team_3' : """
Rotom-Wash @ Leftovers  
Ability: Levitate  
EVs: 252 HP / 168 SpD / 88 Spe  
Calm Nature  
IVs: 0 Atk  
- Thunder Wave  
- Hydro Pump  
- Volt Switch  
- Defog  

Dragapult (M) @ Choice Specs  
Ability: Infiltrator  
EVs: 4 HP / 252 SpA / 252 Spe  
Timid Nature  
- Draco Meteor  
- Shadow Ball  
- Flamethrower  
- U-turn  

Scizor (M) @ Leftovers  
Ability: Technician  
EVs: 244 HP / 60 Def / 204 SpD  
Impish Nature  
IVs: 30 Spe  
- Swords Dance  
- Bullet Punch  
- U-turn  
- Roost  

Landorus-Therian (M) @ Leftovers  
Ability: Intimidate  
EVs: 200 HP / 192 SpD / 116 Spe  
Jolly Nature  
- Earthquake  
- Knock Off  
- U-turn  
- Defog  

Kyurem @ Choice Specs  
Ability: Pressure  
EVs: 4 Def / 252 SpA / 252 Spe  
Modest Nature  
IVs: 0 Atk  
- Ice Beam  
- Freeze-Dry  
- Earth Power  
- Focus Blast  

Bisharp (M) @ Choice Band  
Ability: Defiant  
EVs: 4 HP / 252 Atk / 252 Spe  
Adamant Nature  
- Knock Off  
- Iron Head  
- Beat Up  
- Sucker Punch  
""",

'team_4' : """
Urshifu-Rapid-Strike @ Choice Band  
Ability: Unseen Fist  
EVs: 252 Atk / 4 SpD / 252 Spe  
Jolly Nature  
- Close Combat  
- Surging Strikes  
- U-turn  
- Aqua Jet  

Slowking-Galar @ Assault Vest  
Ability: Regenerator  
Shiny: Yes  
EVs: 252 HP / 136 SpA / 120 SpD  
Calm Nature  
IVs: 0 Atk  
- Future Sight  
- Sludge Bomb  
- Flamethrower  
- Ice Beam  

Ferrothorn @ Leftovers  
Ability: Iron Barbs  
EVs: 252 HP / 144 Def / 112 SpD  
Impish Nature  
- Spikes  
- Knock Off  
- Leech Seed  
- Body Press  

Landorus-Therian @ Leftovers  
Ability: Intimidate  
Shiny: Yes  
EVs: 252 HP / 152 Def / 84 SpD / 20 Spe  
Impish Nature  
- Stealth Rock  
- Earthquake  
- U-turn  
- Toxic  

Tornadus-Therian @ Heavy-Duty Boots  
Ability: Regenerator  
Shiny: Yes  
EVs: 252 HP / 88 Def / 168 Spe  
Timid Nature  
- Hurricane  
- Knock Off  
- U-turn  
- Defog  

Weavile @ Heavy-Duty Boots  
Ability: Pressure  
EVs: 252 Atk / 4 SpD / 252 Spe  
Jolly Nature  
- Swords Dance  
- Knock Off  
- Icicle Crash  
- Ice Shard  
""",

'team_6' : """
Overhead (Tyranitar) @ Leftovers  
Ability: Sand Stream  
EVs: 252 HP / 216 SpD / 40 Spe  
Careful Nature  
- Rock Blast  
- Stealth Rock  
- Protect  
- Earthquake  

Ugh (Excadrill) @ Leftovers  
Ability: Sand Rush  
EVs: 252 Atk / 4 Def / 252 Spe  
Jolly Nature  
- Earthquake  
- Iron Head  
- Swords Dance  
- Rapid Spin  

Dracozolt @ Leftovers  
Ability: Sand Rush  
EVs: 216 Atk / 140 SpA / 152 Spe  
Lonely Nature  
- Bolt Beak  
- Draco Meteor  
- Fire Spin  
- Substitute  

Guns (Tangrowth) @ Rocky Helmet  
Ability: Regenerator  
EVs: 252 HP / 252 Def / 4 SpD  
Relaxed Nature  
- Grass Knot  
- Knock Off  
- Focus Blast  
- Sleep Powder  

Lo-Fight (Slowking) @ Heavy-Duty Boots  
Ability: Regenerator  
EVs: 252 HP / 4 Def / 252 SpD  
Sassy Nature  
IVs: 0 Spe  
- Scald  
- Slack Off  
- Future Sight  
- Teleport  

Stress (Tornadus-Therian) (M) @ Heavy-Duty Boots  
Ability: Regenerator  
EVs: 4 Def / 252 SpA / 252 Spe  
Timid Nature  
IVs: 0 Atk  
- Nasty Plot  
- Hurricane  
- Focus Blast  
- Weather Ball  
""",

'team_7' : """
Pelipper @ Damp Rock  
Ability: Drizzle  
EVs: 248 HP / 252 Def / 8 Spe  
Bold Nature  
- Defog  
- Scald  
- U-turn  
- Roost  

Ferrothorn @ Leftovers  
Ability: Iron Barbs  
EVs: 252 HP / 80 Def / 176 SpD  
Careful Nature  
- Stealth Rock  
- Iron Head  
- Knock Off  
- Spikes  

Barraskewda @ Choice Band  
Ability: Swift Swim  
EVs: 24 HP / 252 Atk / 16 Def / 216 Spe  
Adamant Nature  
- Flip Turn  
- Liquidation  
- Close Combat  
- Crunch  

Zapdos @ Heavy-Duty Boots  
Ability: Static  
EVs: 8 HP / 208 SpA / 52 SpD / 240 Spe  
Timid Nature  
IVs: 0 Atk  
- Thunder  
- Hurricane  
- Weather Ball  
- Roost  

Clefable @ Heavy-Duty Boots  
Ability: Unaware  
EVs: 252 HP / 192 Def / 64 SpD  
Bold Nature  
IVs: 0 Atk  
- Moonblast  
- Thunder Wave  
- Healing Wish  
- Soft-Boiled  

Garchomp @ Leftovers  
Ability: Rough Skin  
EVs: 4 HP / 252 Atk / 252 Spe  
Jolly Nature  
- Swords Dance  
- Earthquake  
- Scale Shot  
- Aqua Tail  
""",

'team_8' : """
Scizor @ Metal Coat  
Ability: Technician  
EVs: 248 HP / 64 Atk / 172 SpD / 24 Spe  
Adamant Nature  
- Swords Dance  
- Bullet Punch  
- Knock Off  
- Roost  

Hydreigon @ Leftovers  
Ability: Levitate  
EVs: 184 HP / 76 SpA / 248 Spe  
Timid Nature  
IVs: 0 Atk  
- Nasty Plot  
- Dark Pulse  
- Earth Power  
- Roost  

Tapu Koko @ Heavy-Duty Boots  
Ability: Electric Surge  
EVs: 12 HP / 32 Def / 212 SpA / 252 Spe  
Timid Nature  
- Thunderbolt  
- Dazzling Gleam  
- U-turn  
- Roost  

Slowking @ Heavy-Duty Boots  
Ability: Regenerator  
EVs: 252 HP / 76 Def / 180 SpD  
Sassy Nature  
IVs: 0 Atk / 0 Spe  
- Scald  
- Future Sight  
- Teleport  
- Slack Off  

Landorus-Therian (M) @ Leftovers  
Ability: Intimidate  
EVs: 252 HP / 128 Def / 104 SpD / 24 Spe  
Impish Nature  
- Stealth Rock  
- Earthquake  
- U-turn  
- Toxic  

Corviknight @ Rocky Helmet  
Ability: Pressure  
EVs: 248 HP / 168 Def / 92 SpD  
Relaxed Nature  
- Body Press  
- U-turn  
- Defog  
- Roost  
""",

'team_9' : """
Melmetal @ Leftovers  
Ability: Iron Fist  
EVs: 252 Atk / 244 SpD / 12 Spe  
Adamant Nature  
- Protect  
- Double Iron Bash  
- Earthquake  
- Toxic  

Zapdos @ Heavy-Duty Boots  
Ability: Static  
EVs: 248 HP / 220 Def / 40 Spe  
Timid Nature  
- Discharge  
- Roost  
- Heat Wave  
- Defog  

Slowking @ Heavy-Duty Boots  
Ability: Regenerator  
EVs: 252 HP / 4 Def / 252 SpD  
Sassy Nature  
IVs: 0 Spe  
- Scald  
- Slack Off  
- Future Sight  
- Teleport  

Landorus-Therian (M) @ Rocky Helmet  
Ability: Intimidate  
EVs: 252 HP / 112 Def / 144 Spe  
Impish Nature  
- Stealth Rock  
- Earthquake  
- U-turn  
- Knock Off  

Dragapult @ Choice Specs  
Ability: Infiltrator  
EVs: 252 SpA / 4 SpD / 252 Spe  
Timid Nature  
- Shadow Ball  
- Draco Meteor  
- U-turn  
- Hex  

Weavile @ Choice Band  
Ability: Pressure  
EVs: 252 Atk / 4 SpD / 252 Spe  
Jolly Nature  
- Triple Axel  
- Knock Off  
- Ice Shard  
- Low Kick  
""",

'team_10' : """
Bisharp @ Choice Band  
Ability: Defiant  
EVs: 252 Atk / 4 SpD / 252 Spe  
Adamant Nature  
- Knock Off  
- Beat Up  
- Iron Head  
- Sucker Punch  

Landorus-Therian (M) @ Leftovers  
Ability: Intimidate  
EVs: 252 HP / 112 Def / 144 Spe  
Impish Nature  
- Stealth Rock  
- Earthquake  
- U-turn  
- Knock Off  

Slowking @ Heavy-Duty Boots  
Ability: Regenerator  
EVs: 252 HP / 4 Def / 252 SpD  
Sassy Nature  
IVs: 0 Spe  
- Scald  
- Slack Off  
- Future Sight  
- Teleport  

Corviknight @ Leftovers  
Ability: Pressure  
EVs: 252 HP / 168 Def / 88 SpD  
Relaxed Nature  
- Defog  
- Roost  
- U-turn  
- Brave Bird  

Zeraora @ Heavy-Duty Boots  
Ability: Volt Absorb  
EVs: 252 Atk / 4 Def / 252 Spe  
Jolly Nature  
- Plasma Fists  
- Volt Switch  
- Close Combat  
- Knock Off  

Clefable @ Life Orb  
Ability: Magic Guard  
EVs: 252 HP / 4 Def / 252 SpD  
Calm Nature  
IVs: 0 Atk  
- Calm Mind  
- Moonblast  
- Soft-Boiled  
- Flamethrower  
""",

'team_11' : """
Tapu Fini @ Leftovers  
Ability: Misty Surge  
EVs: 252 HP / 192 Def / 64 Spe  
Bold Nature  
IVs: 0 Atk  
- Calm Mind  
- Scald  
- Draining Kiss  
- Taunt  

Magnezone @ Leftovers  
Ability: Magnet Pull  
EVs: 252 Def / 172 SpA / 84 Spe  
Bold Nature  
IVs: 0 Atk  
- Iron Defense  
- Body Press  
- Thunderbolt  
- Substitute  

Blissey (F) @ Heavy-Duty Boots  
Ability: Natural Cure  
EVs: 252 HP / 252 Def / 4 SpD  
Bold Nature  
- Seismic Toss  
- Soft-Boiled  
- Thunder Wave  
- Wish  

Melmetal @ Choice Band  
Ability: Iron Fist  
EVs: 32 HP / 252 Atk / 224 Spe  
Adamant Nature  
- Double Iron Bash  
- Earthquake  
- Superpower  
- Thunder Punch  

Dragapult @ Choice Specs  
Ability: Infiltrator  
EVs: 252 SpA / 4 SpD / 252 Spe  
Timid Nature  
- Shadow Ball  
- Draco Meteor  
- U-turn  
- Flamethrower  

Landorus-Therian (M) @ Rocky Helmet  
Ability: Intimidate  
EVs: 252 HP / 112 Def / 144 Spe  
Impish Nature  
- Stealth Rock  
- Earthquake  
- U-turn  
- Knock Off  
"""
}