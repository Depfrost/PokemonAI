from posixpath import basename
import numpy as np

from poke_env.environment.pokemon_type import PokemonType
from poke_env.environment.move_category import MoveCategory
from poke_env.environment.status import Status
from poke_env.data import POKEDEX, MOVES, NATURES
from functools import lru_cache

import requests

@lru_cache(None)
def pokemon_to_int(mon):
    return POKEDEX[mon.species]["num"]

ITEMS = requests.get(
    "https://raw.githubusercontent.com/itsjavi/showdown-data/main/dist/data/items.json"   
).json()["Items"]

ITEM_IDX_MAP = {item: i for i, item in enumerate(sorted(ITEMS))}
ITEM_IDX_MAP[None] = len(ITEM_IDX_MAP)
ITEM_IDX_MAP[""] = len(ITEM_IDX_MAP)
ITEM_IDX_MAP['unknown_item'] = len(ITEM_IDX_MAP)

ABILITIES = requests.get(
    "https://raw.githubusercontent.com/itsjavi/showdown-data/main/dist/data/abilities.json"
).json()["Abilities"]

ABILITIES_IDX_MAP = {ability: i for i, ability in enumerate(sorted(ABILITIES))}
ABILITIES_IDX_MAP[None] = len(ABILITIES_IDX_MAP)
ABILITIES_IDX_MAP[""] = len(ABILITIES_IDX_MAP)

MOVE_IDX_MAP = {move: i for i, move in enumerate(sorted(MOVES))}

def embedEnv(battle):
    # Final vector with 378 components
    return embed_battle(battle)
    
def embed_battle(battle):
    res=np.concatenate((
        # active pokemon
        embed_mon(battle.active_pokemon, battle.opponent_active_pokemon),
        # other pokemons in team
        np.concatenate([embed_mon(mon, battle.opponent_active_pokemon) for mon in battle.team.values() if not mon.active]),
        # opponent active pokemon
        embed_mon(battle.opponent_active_pokemon, battle.active_pokemon)
    ))
    return res

def embed_type(type1, type2 = None):
    base_array = np.zeros(len(PokemonType))
    if type1:
        base_array[type1.value - 1] = 1
    if type2:
        base_array[type2.value - 1] = 1
    return base_array

def embed_status_counter(status, status_counter=0):
    base_array = np.zeros(2)
    if status:
        if (status == Status.TOX):
            base_array[0] = status_counter
        if (status == Status.SLP):
            base_array[1] = status_counter
    return base_array


def embed_status(status):
    base_array = np.zeros(len(Status))
    if status:
        base_array[status.value - 1] = 1
    return base_array


def embed_mon(mon, targetMon=None):
    base_array = np.concatenate((np.array([1]), np.zeros(7)))
    base_array_types= embed_type(None)
    base_array_moves= embed_moves(None)
    base_array_ability= embed_ability(None)
    base_array_item= embed_item(None)
    base_array_status= embed_status(None)
    base_array_status_counter = embed_status_counter(None)
    if mon:
        base_array[0]= mon.current_hp_fraction
        base_array[1]= 1 if mon.active else 0
        base_array[2]= mon.max_hp
        base_array[3]= mon.stats['atk'] if mon.stats['atk'] is not None else mon.base_stats['atk']
        base_array[4]= mon.stats['spa'] if mon.stats['spa'] is not None else mon.base_stats['spa']
        base_array[5]= mon.stats['def'] if mon.stats['def'] is not None else mon.base_stats['def']
        base_array[6]= mon.stats['spd'] if mon.stats['spd'] is not None else mon.base_stats['spd']
        base_array[7]= mon.stats['spe'] if mon.stats['spe'] is not None else mon.base_stats['spe']
        base_array_ability = embed_ability(mon.ability)
        base_array_item= embed_item(mon.item)
        base_array_status= embed_status(mon.status)
        base_array_status_counter= embed_status_counter(mon.status, mon.status_counter)
        base_array_types= embed_type(mon.type_1, mon.type_2)
        base_array_moves= embed_moves(mon.moves, targetMon)
    return np.concatenate([base_array,base_array_ability,base_array_item,base_array_status,base_array_status_counter,base_array_types,base_array_moves])

def embed_moves(moves, targetMon=None):
    move_array = embed_move(None)
    len_move_array= len(move_array)
    base_array = np.tile(move_array,4)
    if moves:
        for count, move in enumerate(moves.values()):
            base_array[count*len_move_array:count*len_move_array+len_move_array]=embed_move(move, targetMon)
    return base_array

def embed_move(move, targetMon=None):
    # TODO: Add move type
    base_array = np.concatenate((np.zeros(4), embed_move_category(None)))
    if move:
        base_array[0]=move.base_power / 100
        base_array[1]=move.accuracy
        base_array[2]=move.heal
        if targetMon:
            base_array[3]=move.type.damage_multiplier(
                targetMon.type_1,
                targetMon.type_2
            )
        base_array[4:]=embed_move_category(move.category)
    return base_array

def embed_move_category(moveCategory):
    base_array = np.zeros(len(MoveCategory))
    if moveCategory:
        base_array[moveCategory.value - 1] = 1
    return base_array

def embed_item(itemName):
    base_array = np.zeros(518) #515 items + 3 for no item(None and "") and unknown item
    if itemName:
        base_array[ITEM_IDX_MAP[itemName]] = 1
    return base_array

def embed_ability(abilityName):
    base_array = np.zeros(272) #271 abilities + 1 for unknown ability
    if abilityName:
        base_array[ABILITIES_IDX_MAP[abilityName.lower().replace(" ", "")]] = 1
    return base_array