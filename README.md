# PokemonAI

The goal of this project is to build an AI capable of battling on Pokemon Showdown.
The targeted battle format is 8th gen OU.

## Installation

This project requires Python 3.6 and uses [poke-env](https://github.com/hsahovic/poke-env)
```
pip install requirements.txt
pip install --no-deps keras-rl2==1.0.3
```
In your Python installation folder go to ./site-packages/rl/agents/dqn.py
And replace the following line (should be line 107):
```
if hasattr(model.output, '__len__') and len(model.output) > 1:
```
by
```
if hasattr(model.output, '__len__') and len([model.output.shape.dims.__len__()]) > 1:
```

This project also requires a [Pokemon Showdown](https://github.com/smogon/pokemon-showdown) server :
```
git clone https://github.com/smogon/pokemon-showdown.git
cd pokemon-showdown
npm install
cp config/config-example.js config/config.js
sudo node pokemon-showdown start --no-security
```

## How to use


```python
# Training
python learningPlayer.py
```
This will train the AI. By default the program will train the DQN agent for 10 000 steps against itself. It can achieve better result with longer training times. The trained model is saved in the folder **model_10000**.
```python
# Challenge
python showdownLive.py
```
To will allow you to battle the trained AI on the real [showdown server](https://play.pokemonshowdown.com/). In order to do that you need to challenge the user **ushioplayspokemon**. You can also easily modify the code to make the bot play on the ladder or send challenges.