# -*- coding: utf-8 -*-
import numpy as np

from poke_env.player.player import Player
from simpleRLPlayer import SimpleRLPlayer

class TrainedRLPlayer(Player):
  def __init__(self, model, *args, **kwargs):
    Player.__init__(self, *args, **kwargs)
    self.model = model

  def choose_move(self, battle):
    state =  SimpleRLPlayer.embed_battle(self, battle)
    predictions = self.model.predict(np.expand_dims([state], 0))[0]
    action = np.argmax(predictions)
    return SimpleRLPlayer._action_to_move(self, action, battle)
