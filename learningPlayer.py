# -*- coding: utf-8 -*-
import numpy as np
import tensorflow as tf

from poke_env.player.random_player import RandomPlayer

from rl.agents.dqn import DQNAgent
from rl.policy import LinearAnnealedPolicy, EpsGreedyQPolicy
from rl.memory import SequentialMemory
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam

from teambuilder import custom_builder
from maxDamagePlayer import MaxDamagePlayer
from maxDamagePlayerTyped import MaxDamagePlayerTyped
from trainedRLPlayer import TrainedRLPlayer
from simpleRLPlayer import SimpleRLPlayer
from poke_env.server_configuration import ServerConfiguration
from os import path

NB_TRAINING_STEPS = 10000
NB_EVALUATION_EPISODES = 100
MODEL_NAME = "model_{}".format(NB_TRAINING_STEPS)

tf.random.set_seed(0)
np.random.seed(0)


# This is the function that will be used to train the dqn
def dqn_training(player, dqn, nb_steps, callbacks):
    dqn.fit(player, nb_steps=nb_steps, callbacks=callbacks)
    player.complete_current_battle()


def dqn_evaluation(player, dqn, nb_episodes):
    # Reset battle statistics
    player.reset_battles()
    dqn.test(player, nb_episodes=nb_episodes, visualize=False, verbose=False)

    print(
        "DQN Evaluation: %d victories out of %d episodes"
        % (player.n_won_battles, nb_episodes)
    )

def selftrain(nb_trainings):
    for nb_training in range(nb_trainings):
        print("training number:%d" % (nb_training))
        env_player = SimpleRLPlayer(
            battle_format="gen8ou",
            team=custom_builder,
            server_configuration=ServerConfiguration(
                "localhost:8000",
                "authentication-endpoint.com/action.php?"
            )
        )

        opp1 = RandomPlayer(
            battle_format="gen8ou",
            team=custom_builder,
            server_configuration=ServerConfiguration(
                "localhost:8000",
                "authentication-endpoint.com/action.php?"
            )
        )
        opp2  = MaxDamagePlayer(
            battle_format="gen8ou",
            team=custom_builder,
            server_configuration=ServerConfiguration(
                "localhost:8000",
                "authentication-endpoint.com/action.php?"
            )
        )
        opp3= MaxDamagePlayerTyped(
            battle_format="gen8ou",
            team=custom_builder,
            server_configuration=ServerConfiguration(
                "localhost:8000",
                "authentication-endpoint.com/action.php?"
            )
        )
        
        # Output dimension
        n_action = len(env_player.action_space)

        model = Sequential()
        model.add(Dense(128, activation="elu", input_shape=(1, 5971,)))

        # Our embedding have shape (1, 10), which affects our hidden layer
        # dimension and output dimension
        # Flattening resolve potential issues that would arise otherwise
        model.add(Flatten())
        model.add(Dense(64, activation="elu"))
        model.add(Dense(n_action, activation="linear"))

        memory = SequentialMemory(limit=NB_TRAINING_STEPS, window_length=1)

        # Simple epsilon greedy
        policy = LinearAnnealedPolicy(
            EpsGreedyQPolicy(),
            attr="eps",
            value_max=1.0,
            value_min=0.05,
            value_test=0,
            nb_steps=NB_TRAINING_STEPS,
        )
        
        model2=tf.keras.models.load_model(MODEL_NAME) if path.exists(MODEL_NAME) else model

        # Defining our DQN
        dqn = DQNAgent(
            model=model2,
            nb_actions=len(env_player.action_space),
            policy=policy,
            memory=memory,
            nb_steps_warmup=NB_TRAINING_STEPS/10,
            gamma=0.5,
            target_model_update=1,
            delta_clip=0.01,
            enable_double_dqn=True,
            #enable_dueling_network=True,
            #dueling_type="avg"
        )

        dqn.compile(Adam(lr=0.00025), metrics=["mae"])
        
        opp4 = TrainedRLPlayer(
            model=model2,
            battle_format="gen8ou",
            team=custom_builder,
            server_configuration=ServerConfiguration(
                "localhost:8000",
                "authentication-endpoint.com/action.php?"
            )
        )

        train(env_player,dqn,opp4)
        model2.save("model_%d" % NB_TRAINING_STEPS)
        evaluate(env_player, dqn, opp1,opp2,opp3,opp4)
        #if (env_player.n_won_battles > 50):
        #    model2.save("model_%d" % NB_TRAINING_STEPS)
        #    print("Won {} battles. Model saved.".format(env_player.n_won_battles))
        del(model2)


def train(env_player, dqn, opp):
    callbacks = list()

    # Training
    env_player.play_against(
        env_algorithm=dqn_training,
        opponent=opp,
        env_algorithm_kwargs={"dqn": dqn, "nb_steps": NB_TRAINING_STEPS, "callbacks":callbacks},
    )

# Evaluation
def evaluate(env_player,dqn,opp1,opp2,opp3,opp4):
    print("Results against random player:")
    env_player.play_against(
        env_algorithm=dqn_evaluation,
        opponent=opp1,
        env_algorithm_kwargs={"dqn": dqn, "nb_episodes": NB_EVALUATION_EPISODES},
    )

    print("\nResults against max player:")
    env_player.play_against(
        env_algorithm=dqn_evaluation,
        opponent=opp2,
        env_algorithm_kwargs={"dqn": dqn, "nb_episodes": NB_EVALUATION_EPISODES},
    )

    print("\nResults against max player typed:")
    env_player.play_against(
        env_algorithm=dqn_evaluation,
        opponent=opp3,
        env_algorithm_kwargs={"dqn": dqn, "nb_episodes": NB_EVALUATION_EPISODES},
    )

    """
    print("\nResults against last trained player:")
    env_player.play_against(
        env_algorithm=dqn_evaluation,
        opponent=opp4,
        env_algorithm_kwargs={"dqn": dqn, "nb_episodes": NB_EVALUATION_EPISODES},
    )
    """

if __name__ == "__main__":
    selftrain(1)